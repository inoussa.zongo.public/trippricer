package com.inoussa.zongo.trippricer.exception;

public class TripPricerException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public TripPricerException(String message) {
		super(message);
	}
	
	public TripPricerException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public TripPricerException(Throwable cause) {
		super(cause);
	}

}
