package com.inoussa.zongo.trippricer.exception;

public class ZoneNotFoundException extends TripPricerException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ZoneNotFoundException(String message) {
		super(message);
	}
	
	public ZoneNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public ZoneNotFoundException(Throwable cause) {
		super(cause);
	}
	
}
