package com.inoussa.zongo.trippricer.pricer;

import com.inoussa.zongo.trippricer.model.Price;
import com.inoussa.zongo.trippricer.model.StationNet;

/**
 * Pricing interface
 * @author inous
 *
 */
public interface IPricer {
	/**
	 * price a trip given the start station, the end staton and the stations network structure
	 * @param fromStation the station where the trip begin
	 * @param toStation the station where the trip end
	 * @param net the station network structure 
	 * @return the {@link Price} of the trip
	 */
	public Price price(String fromStation, String toStation, StationNet net);
	
	/**
	 * Calculate the cost of a trip
	 * @param zoneStart the zone were the trip started
	 * @param zoneEnd the zone where the trip ended
	 * @return the cost of the trip
	 */
	public  Integer price(Integer zoneStart, Integer zoneEnd);
}
