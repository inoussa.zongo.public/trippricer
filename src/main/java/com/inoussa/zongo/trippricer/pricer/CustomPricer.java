package com.inoussa.zongo.trippricer.pricer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.inoussa.zongo.trippricer.model.Price;
import com.inoussa.zongo.trippricer.model.StationNet;
import com.inoussa.zongo.trippricer.model.Zone;

/**
 * A implementation of the pricing methods
 * 
 * @author inous
 *
 */
public class CustomPricer implements IPricer {

	@Override
	/**
	 * price a trip given the start station, the end staton and the stations network
	 * structure
	 * 
	 * @param fromStation the station where the trip begin
	 * @param toStation   the station where the trip end
	 * @param net         the station network structure
	 * @return the {@link Price} of the trip
	 */
	public Price price(String fromStation, String toStation, StationNet net) {
		List<Price> prices = new ArrayList<>();

		Set<Zone> fromZones = net.getStationZones(fromStation).stream().filter(fz -> fz != null)
				.collect(Collectors.toSet());
		Set<Zone> toZones = net.getStationZones(toStation).stream().filter(tz -> tz != null)
				.collect(Collectors.toSet());

		fromZones.stream().forEach(fz -> {
			toZones.stream().forEach(tz -> {
				Price price = new Price();
				int cost = price(fz.getName(), tz.getName());

				price.setStartZone(fz.getName());
				price.setEndZone(tz.getName());
				price.setValue(cost);
				prices.add(price);
			});
		});

		return prices.stream().min(Comparator.comparing(Price::getValue)).orElse(null);
	}

	@Override
	/**
	 * Calculate the cost of a trip
	 * @param zoneStart the zone were the trip started
	 * @param zoneEnd the zone where the trip ended
	 * @return the cost of the trip
	 */
	public  Integer price(Integer zoneStart, Integer zoneEnd) {
		int cost = 0;
		
		//within zone 1 and zone 2
		if ((zoneStart == 1 || zoneStart == 2) && (zoneEnd == 1 || zoneEnd == 2)) {
				cost = 240;
				//within zone 2 and zone 4
			} else if ((zoneStart == 3 || zoneStart == 4) && (zoneEnd == 3 || zoneEnd == 4)) {
				cost = 200;
				//from zone 3 to zone 1 or zone 2
			} else if (zoneStart == 3 && (zoneEnd == 1 || zoneEnd == 2)) {
				cost = 280;
				//from zone 4 to zone 1 or zone 2
			} else if (zoneStart == 4 && (zoneEnd == 1 || zoneEnd == 2)) {
				cost = 300;
				//from zone 1 or zone 2 to zone 3
			} else if ((zoneStart == 1 || zoneStart == 2) && zoneEnd == 3) {
				cost = 280;
				//from zone 1 or zone 2 to zone 4
			} else if ((zoneStart == 1 || zoneStart == 2) && zoneEnd == 4) {
				cost = 300;
			}
		return cost;
}

}
