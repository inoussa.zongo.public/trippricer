package com.inoussa.zongo.trippricer.io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * {@link JsonParser} provide methods for reading/writing json string or file
 * @author inous
 *
 */
public class JsonParser {
	
	/**
	 * 
	 * @param fileName the file name contening json to read
	 * @param clazz the class to map the readed json
	 * @return the readed json as object mapped to specified class
	 * @throws JsonParseException if malformed Json
	 * @throws JsonMappingException if error on mapping json to specified class
	 * @throws IOException any io error
	 */
	public  <T> T getObjects(String fileName, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException {
		T object = null;
		ObjectMapper mapper = new ObjectMapper();
		object = mapper.readValue(Paths.get(fileName).toFile(), clazz);
		return object;
	}
	
	/**
	 * Convert a object to json string
	 * @param object Object to convert to json
	 * @return Json string corresponding to specified object
	 */
	public <T> String writeToString(T object) {
		ObjectMapper mapper = new ObjectMapper();
		String out = "";
		try {
			out = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
		} catch (JsonProcessingException e) {
			System.err.println(e.getMessage());
		}
		return out;
	}
	
	/**
	 * Convert object to json string and write that string to file
	 * @param object Object to convert to json string
	 * @param fileName file name where to write the json
	 * @throws IOException 
	 */
	public void writeToFile(Object object, String fileName) throws IOException {
			String content = writeToString(object);
			Files.write(Paths.get(fileName), content.getBytes(StandardCharsets.UTF_8));
	}
	
}
