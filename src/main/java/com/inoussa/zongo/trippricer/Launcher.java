package com.inoussa.zongo.trippricer;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.inoussa.zongo.trippricer.io.JsonParser;
import com.inoussa.zongo.trippricer.model.CustomerSumary;
import com.inoussa.zongo.trippricer.model.Price;
import com.inoussa.zongo.trippricer.model.Station;
import com.inoussa.zongo.trippricer.model.StationNet;
import com.inoussa.zongo.trippricer.model.Tap;
import com.inoussa.zongo.trippricer.model.Trip;
import com.inoussa.zongo.trippricer.model.Zone;
import com.inoussa.zongo.trippricer.model.view.CustomerSumaryView;
import com.inoussa.zongo.trippricer.model.view.TapView;
import com.inoussa.zongo.trippricer.pricer.CustomPricer;
import com.inoussa.zongo.trippricer.pricer.IPricer;

public class Launcher {
	// A static member that contain the train station network
	private static StationNet stationNet;

	public static void main(String[] args) {
		
		//If the requested arguments absent, the program exits
		if (args.length < 2) {
			System.err.println("Incorect number of program arguments");
			System.err.println("Please give as first argument the input file name and as second argument the output file one");
			return;
		}
		//
		String inputFileName = args[0];
		String outputFileName = args[1];

		//Creating the train network
		generateStationNet();

		/*
		 * Here we parse the input file et map it to the corresponding class
		 * If some error occure, the program exits
		 */
		JsonParser parser = new JsonParser();
		TapView tapsCollection = null;
		try {
			tapsCollection = parser.getObjects(inputFileName, TapView.class);
		} catch (JsonParseException e) {
			System.err.println("Error occured on reading file "+inputFileName);
			System.err.println("Please check the file is well formated according to json standard");
			return;
		}catch (JsonMappingException e) {
			e.printStackTrace();
			System.err.println("Error occured on reading file "+inputFileName);
			return;
		}catch (IOException e) {
			System.err.println("Error occured on reading file "+inputFileName);
			System.err.println("Please check the file is accessible");
			return;
		}

		//Grouping taps per customer
		Map<Long, List<Tap>> customerTaps = tapsCollection.getTaps().stream()
				.collect(Collectors.groupingBy(Tap::getCustomerId));

		//Creation of pricer Object and the view of cutomer sumary to meet output file purpose
		IPricer pricer = new CustomPricer();
		CustomerSumaryView csc = new CustomerSumaryView();

		//Pricing and creating customer summary for each customer
		customerTaps.forEach((customerId, cutomerTaps) -> {
			Collections.sort(cutomerTaps);
			CustomerSumary customerSumary = new CustomerSumary();
			customerSumary.setCustomerId(customerId);
			for (int i = 0; i < cutomerTaps.size(); i++) {
				Trip trip = new Trip();
				trip.setStationStart(cutomerTaps.get(i).getStation().getName());
				trip.setStationEnd(cutomerTaps.get(i + 1).getStation().getName());
				trip.setStartedJourneyAt(cutomerTaps.get(i).getTimeStamp());

				Price price = pricer.price(trip.getStationStart(), trip.getStationEnd(), stationNet);
				trip.setCostInCents(price.getValue());
				trip.setZoneFrom(price.getStartZone());
				trip.setZoneTo(price.getEndZone());
				customerSumary.addTrip(trip);
				i++;
			}
			csc.add(customerSumary);
		});

		//Calculatinf total for each customer whole trips
		for (CustomerSumary cs : csc.getCustomerSummaries()) {
			int totalCost = cs.getTrips().stream().map(c -> c.getCostInCents()).reduce(0, (t, e) -> t + e);
			cs.setTotalCostInCents(totalCost);
		}

		//Writing customer summary to output file
		try {
			parser.writeToFile(csc, outputFileName);
		} catch (IOException e) {
			System.err.println(String.format("Error occured when writting on file %s", outputFileName));
			System.err.println("Please, Cheick the file name is correct.");
			System.err.println(e.getMessage());
			return;
		}
		
		System.out.println("\n\tEvery thing worked correctely !!");
		System.out.println("\tGet the result here : "+ Paths.get(outputFileName).toAbsolutePath() + "\t");
	}

	/**
	 * Static method creating the train netork
	 * @return
	 */
	private static StationNet generateStationNet() {
		if (stationNet != null)
			return stationNet;
		// Zones
		Zone zone1 = new Zone(1);
		Zone zone2 = new Zone(2);
		Zone zone3 = new Zone(3);
		Zone zone4 = new Zone(4);
		
		//Zone's integration
		zone1.setNextZone(zone2);
		zone2.setNextZone(zone3);
		zone3.setNextZone(zone4);
		
		// Stations
		Station stationA = new Station("A");
		Station stationB = new Station("B");
		Station stationC = new Station("C");
		Station stationD = new Station("D");
		Station stationE = new Station("E");
		Station stationF = new Station("F");
		Station stationG = new Station("G");
		Station stationH = new Station("H");
		Station stationI = new Station("I");
		
		//Zone 1 stations
		zone1.addStation(stationA);
		zone1.addStation(stationB);

		//Zone 2 stations
		zone2.addStation(stationC);
		zone2.addStation(stationD);
		zone2.addStation(stationE);

		//Zone 3 stations
		zone3.addStation(stationC);
		zone3.addStation(stationE);
		zone3.addStation(stationF);

		//Zone 4 stations
		zone4.addStation(stationF);
		zone4.addStation(stationG);
		zone4.addStation(stationH);
		zone4.addStation(stationI);
		// Net
		stationNet = new StationNet(zone1);
		
		return stationNet;
	}

}
