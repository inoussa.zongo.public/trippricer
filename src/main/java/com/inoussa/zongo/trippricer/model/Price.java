package com.inoussa.zongo.trippricer.model;

import java.util.Comparator;

/**
 * A class for representing a price of a trip
 * @author inous
 *
 */
public class Price implements Comparator<Price>{
	
	/**
	 * The value of the price
	 */
	private Integer value;
	
	/**
	 * the start zone for which  the price of the trip is calculated
	 */
	private Integer startZone;
	
	/**
	 * the end zone for which  the price of the trip is calculated
	 */
	private Integer endZone;
	
	public Price() {
		// TODO Auto-generated constructor stub
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getStartZone() {
		return startZone;
	}

	public void setStartZone(Integer startZone) {
		this.startZone = startZone;
	}

	public Integer getEndZone() {
		return endZone;
	}

	public void setEndZone(Integer endZone) {
		this.endZone = endZone;
	}

	@Override
	public int compare(Price o1, Price o2) {
		return o1.getValue() - o2.getValue();
	}
	
}
