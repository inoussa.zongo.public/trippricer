package com.inoussa.zongo.trippricer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *Model class for a trip on the train network
 * @author inous
 *
 */
public class Trip implements Comparable<Trip> {
	
	/**
	 * the name of the station the trip started
	 */
	@JsonProperty
	private String stationStart;
	
	/**
	 * the name of the station the trip end
	 */
	@JsonProperty
	private String stationEnd;
	
	/**
	 * the date (timestamp) when the trip get started
	 */
	@JsonProperty
	private Long startedJourneyAt;
	
	/**
	 * the cost of the trip
	 */
	@JsonProperty
	private Integer costInCents;
	
	/**
	 * the zone where the trip get started
	 */
	@JsonProperty
	private Integer zoneFrom;
	
	/**
	 * the zone where the trip end
	 */
	@JsonProperty
	private Integer zoneTo;
	
	public Trip() {
		// TODO Auto-generated constructor stub
	}


	public String getStationStart() {
		return stationStart;
	}

	public void setStationStart(String stationStart) {
		this.stationStart = stationStart;
	}

	public String getStationEnd() {
		return stationEnd;
	}

	public void setStationEnd(String stationEnd) {
		this.stationEnd = stationEnd;
	}

	public Long getStartedJourneyAt() {
		return startedJourneyAt;
	}

	public void setStartedJourneyAt(Long startedJourneyAt) {
		this.startedJourneyAt = startedJourneyAt;
	}

	public Integer getCostInCents() {
		return costInCents;
	}

	public void setCostInCents(Integer costInCents) {
		this.costInCents = costInCents;
	}

	public Integer getZoneFrom() {
		return zoneFrom;
	}

	public void setZoneFrom(Integer zoneFrom) {
		this.zoneFrom = zoneFrom;
	}

	public Integer getZoneTo() {
		return zoneTo;
	}

	public void setZoneTo(Integer zoneTo) {
		this.zoneTo = zoneTo;
	}

	@Override
	public int compareTo(Trip o) {
		long diff = this.startedJourneyAt - o.startedJourneyAt;
		if(diff < 0) return -1;
		else if (diff > 0) {
			return 1;
		}else {
			return 0;
		}
	}
	
}
