package com.inoussa.zongo.trippricer.model.view;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inoussa.zongo.trippricer.model.Tap;

/**
 * A convenient class to map a customer {@link Tap} from the input file
 * @author inous
 *
 */
public class TapView {
	
	/**
	 * customers taps
	 */
	@JsonProperty
	List<Tap> taps = new ArrayList<>();
	
	public TapView() {
		
	}

	public List<Tap> getTaps() {
		return taps;
	}

	public void setTaps(List<Tap> taps) {
		this.taps = taps;
	}
	
}
