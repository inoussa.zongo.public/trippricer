package com.inoussa.zongo.trippricer.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Representation of a Zone
 * @author inous
 *
 */
public class Zone {
	/**
	 * the zone name
	 */
	private Integer name;
	
	/**
	 * set of station within the zone
	 */
	private Set<Station> stations = new HashSet<>();
	
	/**
	 * the zone neighboring outdoor zone
	 */
	private Zone nextZone;
	
	
	public Zone(Integer name) {
		this.name = name;
	}
	
	public Integer getName() {
		return name;
	}

	public void setName(Integer name) {
		this.name = name;
	}

	public Zone getNextZone() {
		return nextZone;
	}

	public void setNextZone(Zone nextZone) {
		this.nextZone = nextZone;
	}

	public Set<Station> getStations() {
		return stations;
	}

	public void addStation(Station station) {
		this.stations.add(station);
	}
	
}
