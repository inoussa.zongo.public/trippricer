package com.inoussa.zongo.trippricer.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author inous
 *
 *A class to represent a station
 */
public class Station {
	/** The name of the station */
	@JsonProperty
	private String name;
	
	/**
	 * Defaut constructor
	 * This constuctor is made private to avoid constructing a station with no attributes
	 */
	@SuppressWarnings("unused")
	private Station() {
		
	}
	
	/**
	 * Initializes a new {@code Station} with the given name
	 * The created station is not boundary
	 * @param name
	 */
	public Station(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!(obj instanceof Station)) return false;
		Station station = (Station)obj;
		return this.name.equals(station.getName());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.name);
	}
}
