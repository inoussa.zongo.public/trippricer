package com.inoussa.zongo.trippricer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A class gathering data for customer tap on the train network.<br>
 * @author inous
 *
 */
public class Tap implements Comparable<Tap>{
	
	/**
	 * the customer id
	 */
	@JsonProperty
	private Long customerId;
	
	/**
	 * date (timestamp) when the customer taps
	 */
	@JsonProperty
	private Long unixTimestamp;
	
	/**
	 * the {@link Station} where the customer taps
	 */
	@JsonProperty
	private Station station;
	
	
	public Tap() {
		// TODO Auto-generated constructor stub
	}
	
	public Tap(Long customerId, Station station, Long timeStamp) {
		this.customerId = customerId;
		this.station = station;
		this.unixTimestamp = timeStamp;
	}
	
	public Long getTimeStamp() {
		return unixTimestamp;
	}
	public void setTimeStamp(Long timeStamp) {
		this.unixTimestamp = timeStamp;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	
	@Override
	public String toString() {
		return String.format("Taps[customerId = %d - Station = %s - timeStamp = %d]", customerId, station, unixTimestamp);
	}

	@Override
	public int compareTo(Tap o) {
		long diff = this.unixTimestamp - o.unixTimestamp;
		if(diff < 0) return -1;
		else if (diff > 0) {
			return 1;
		}else {
			return 0;
		}
	}
	
}
