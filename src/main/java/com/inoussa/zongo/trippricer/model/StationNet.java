package com.inoussa.zongo.trippricer.model;

import java.util.HashSet;
import java.util.Set;

import com.inoussa.zongo.trippricer.exception.ZoneNotFoundException;

/**
 * {@link StationNet} expose methods for search purpose in a train network
 * @author inous
 *
 */
public class StationNet {
	
	/**
	 * The deepest zone among zones<br>
	 * From this zone, we can browse the whole train network
	 */
	private Zone innerZone;
	
	@SuppressWarnings("unused")
	private StationNet() {
		
	}
	
	public StationNet(Zone innerZone) {
		this.innerZone = innerZone;
	}
	
	/**
	 * Determinates if a station in in boundary of two zones
	 * @param stationName the name of the station
	 * @return <code>true</code> if the station is boundary of two zones, <code>false</code> if not
	 */
	public boolean isStationBorder(String stationName) {
		return getStationZones(stationName).size() == 2 ? true : false;
	}
	
	/**
	 * find zones a station is situated
	 * @param stationName the name of the station
	 * @return the zones within the station is situated
	 */
	public Set<Zone> getStationZones(String stationName) /**throws ZoneNotFoundException*/ {
		Zone currentZone = innerZone;
		Set<Zone> zones = new HashSet<>();
		while(currentZone != null) {
			Station station = currentZone.getStations().stream().filter(s -> s.getName().equals(stationName)).findFirst().orElse(null);
			if(station != null) zones.add(currentZone);
			currentZone = currentZone.getNextZone();
		}
		
		//if(zones.isEmpty()) throw new ZoneNotFoundException(String.format("No zone found for station %s in the train network", stationName));
		
		return zones;
	}
	
	/**
	 * find zone by name in the train network
	 * @param zoneName the name of the zone
	 * @return the zone
	 * @throws ZoneNotFoundException if no zone found for the given zone name
	 */
	public Zone getZoneByName(Integer zoneName) throws ZoneNotFoundException {
		Zone currentZone = innerZone;
		Zone zone = null;
		while(zone == null && currentZone != null) {
			zone = currentZone.getName().equals(zoneName) ? currentZone : null;
			currentZone = currentZone.getNextZone();
		}
		
		if(zone == null) throw new ZoneNotFoundException(String.format("No zone with name %d found in the train network", zoneName));
		
		return zone;
	}
	
	public Zone getInnerZone() {
		return innerZone;
	}

	public void setInnerZone(Zone innerZone) {
		this.innerZone = innerZone;
	}
	
}
