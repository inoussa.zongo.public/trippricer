package com.inoussa.zongo.trippricer.model.view;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inoussa.zongo.trippricer.model.CustomerSumary;

/**
 * A convenient class to map a {@link CustomerSumary} to the output file
 * @author inous
 *
 */
public class CustomerSumaryView {
	
	@JsonProperty
	private List<CustomerSumary> customerSummaries = new ArrayList<>();
	
	public CustomerSumaryView() {
		// TODO Auto-generated constructor stub
	}

	public List<CustomerSumary> getCustomerSummaries() {
		return customerSummaries;
	}
	
	public void add(CustomerSumary cs) {
		this.customerSummaries.add(cs);
		}
	
}
