package com.inoussa.zongo.trippricer.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A class representing a customer trip summary in the train network
 * @author inous
 *
 */
public class CustomerSumary {
	
	/**
	 * the customer identifier
	 */
	@JsonProperty
	private Long customerId;
	
	/**
	 * the total cost of the customer trips
	 */
	@JsonProperty
	private Integer totalCostInCents;
	
	/**
	 * the customer trip collections
	 */
	@JsonProperty
	List<Trip> trips = new ArrayList<>();
	
	public CustomerSumary() {
		// TODO Auto-generated constructor stub
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Integer getTotalCostInCents() {
		return totalCostInCents;
	}

	public void setTotalCostInCents(Integer totalCostInCents) {
		//Indicate if customer travels within zone 1 and 2
		boolean isTravelWithinZone1And2 = getTrips().stream().allMatch(trip-> trip.getZoneFrom() <3 && trip.getZoneTo() <3);
				
		//Indicate if customer travels within zone 1 and 2
		boolean isTravelWithinZone3And4 = getTrips().stream().allMatch(trip-> trip.getZoneFrom() >2 && trip.getZoneTo() > 2);
				
		//Travel costs 600 if customer travels within zone 1 and 2
		totalCostInCents = isTravelWithinZone1And2 && !getTrips().isEmpty() && totalCostInCents > 600 ? 600 : totalCostInCents;
				
		//Travel costs 800 if customer travels within zone 3 and 4
		totalCostInCents = isTravelWithinZone3And4 && !getTrips().isEmpty() && totalCostInCents > 800 ? 800 : totalCostInCents;
		
		//Travel costs definitively 1000 if initial cost is more than 1000
		totalCostInCents = totalCostInCents >1000 ? 1000 : totalCostInCents;
		
		this.totalCostInCents = totalCostInCents;
	}

	public List<Trip> getTrips() {
		return trips;
	}
	
	public void addTrip(Trip trip) {
		this.trips.add(trip);
	}
	
	@Override
	public String toString() {
		return String.format("Customer[id = %d]", customerId);
	}
}
