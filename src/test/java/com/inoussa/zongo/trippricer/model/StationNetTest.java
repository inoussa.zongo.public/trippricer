package com.inoussa.zongo.trippricer.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.inoussa.zongo.trippricer.exception.ZoneNotFoundException;

public class StationNetTest {
	public StationNet net;
	
	@BeforeEach
	public void setUp() {
		//Zones creation
		Zone zone1 = new Zone(1);
		Zone zone2 = new Zone(2);
		Zone zone3 = new Zone(3);
		Zone zone4 = new Zone(4);
		
		// Stations cr�ation
		Station stationA = new Station("A");
		Station stationB = new Station("B");
		Station stationC = new Station("C");
		Station stationD = new Station("D");
		Station stationE = new Station("E");
		Station stationF = new Station("F");
		Station stationG = new Station("G");
		Station stationH = new Station("H");
		Station stationI = new Station("I");
		
		
		//Zones inclusion
		zone1.setNextZone(zone2);
		zone2.setNextZone(zone3);
		zone3.setNextZone(zone4);
		
		//Zone 1 Stations
		zone1.addStation(stationA);
		zone1.addStation(stationB);
		
		//Zone 2 stations
		zone2.addStation(stationC);
		zone2.addStation(stationD);
		zone2.addStation(stationE);
		
		//Zone  3 station
		zone3.addStation(stationC);
		zone3.addStation(stationE);
		zone3.addStation(stationF);
		
		//Zone 4 stations
		zone4.addStation(stationF);
		zone4.addStation(stationG);
		zone4.addStation(stationH);
		zone4.addStation(stationI);
		
		//Network 
		net = new StationNet(zone1);
	}
	
	@Test
	public void shouldGetZoneWithName1ReturnZone1() {
		Zone zone = net.getZoneByName(1);
		assertEquals(1, zone.getName());
	}
	
	@Test
	public void shouldGetZoneWithName10ThrowsZoneNotFoundException() {
		assertThrows(ZoneNotFoundException.class, ()->{
			net.getZoneByName(10);
		});
	}
	
	
	@Test
	public void shouldStationWithNameANotBorder() {
		boolean isBorder = net.isStationBorder("A");
		assertFalse(isBorder);
	}
	
	@Test
	public void shouldStationWithNameCBeBorder() {
		boolean isBorder = net.isStationBorder("C");
		assertTrue(isBorder);
	}
	
	@Test
	public void shouldStationAZoneBeOnlyZone1() {
		Set<Zone> zones = net.getStationZones("A");
		assertEquals(1, zones.size());
		assertEquals(1, zones.iterator().next().getName());
	}
	
	@Test
	public void shouldStationFZonesSizeBe2() {
		Set<Zone> zones = net.getStationZones("F");
		//List<Integer> zonesNames = zones.stream().map(z->z.getName()).collect(Collectors.toList());
		assertEquals(2, zones.size());
	}
}
