package com.inoussa.zongo.trippricer.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.inoussa.zongo.trippricer.model.CustomerSumary;

public class CustomerSumaryTest {
	
	CustomerSumary cs;
	
	@BeforeEach
	public void setUp() {
		cs = new CustomerSumary();
	}
	
	@Test
	public void shouldTravelCost1000WhenTotalCostIs1000() {
		cs.setTotalCostInCents(1000);
		assertEquals(1000, cs.getTotalCostInCents());
	}
	
	@Test
	public void shouldTravelCost1000WhenTotalCost1200() {
		cs.setTotalCostInCents(1200);
		assertEquals(1000, cs.getTotalCostInCents());
	}
	
	@Test
	public void shouldCustomerTravelCost800WhenTotalCost800() {
		cs.setTotalCostInCents(800);
		assertEquals(800, cs.getTotalCostInCents());
	}
	

	@Test
	public void shouldTravelCost600WhenTravelWithInZone1And2() {
		Trip trip = new Trip();
		trip.setZoneFrom(1);
		trip.setZoneTo(2);
		cs.addTrip(trip);
		cs.setTotalCostInCents(700);
		assertEquals(600, cs.getTotalCostInCents());
	}
	
	@Test
	public void shouldCustomerJourneyCost800WhenTravelWithInZone3And4() {
		Trip trip = new Trip();
		trip.setZoneFrom(3);
		trip.setZoneTo(4);
		cs.addTrip(trip);
		
		cs.setTotalCostInCents(900);
		assertEquals(800, cs.getTotalCostInCents());
	}
	
	@Test
	public void shouldJourneyCost800WhenTravelWithInZone3And4AndTotalCostIs1200() {
		Trip trip = new Trip();
		trip.setZoneFrom(3);
		trip.setZoneTo(4);
		cs.addTrip(trip);
		
		cs.setTotalCostInCents(1200);
		assertEquals(800, cs.getTotalCostInCents());
	}
	
}
