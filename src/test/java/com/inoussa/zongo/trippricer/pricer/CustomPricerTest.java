package com.inoussa.zongo.trippricer.pricer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.inoussa.zongo.trippricer.model.Price;
import com.inoussa.zongo.trippricer.model.Station;
import com.inoussa.zongo.trippricer.model.StationNet;
import com.inoussa.zongo.trippricer.model.Zone;

public class CustomPricerTest {
public StationNet net;
public IPricer pricer;
	
	@BeforeEach
	public void setUp() {
		//Zones creation
		Zone zone1 = new Zone(1);
		Zone zone2 = new Zone(2);
		Zone zone3 = new Zone(3);
		Zone zone4 = new Zone(4);
		
		// Stations cr�ation
		Station stationA = new Station("A");
		Station stationB = new Station("B");
		Station stationC = new Station("C");
		Station stationD = new Station("D");
		Station stationE = new Station("E");
		Station stationF = new Station("F");
		Station stationG = new Station("G");
		Station stationH = new Station("H");
		Station stationI = new Station("I");
		
		
		//Zones inclusion
		zone1.setNextZone(zone2);
		zone2.setNextZone(zone3);
		zone3.setNextZone(zone4);
		
		//Zone 1 Stations
		zone1.addStation(stationA);
		zone1.addStation(stationB);
		
		//Zone 2 stations
		zone2.addStation(stationC);
		zone2.addStation(stationD);
		zone2.addStation(stationE);
		
		//Zone  3 station
		zone3.addStation(stationC);
		zone3.addStation(stationE);
		zone3.addStation(stationF);
		
		//Zone 4 stations
		zone4.addStation(stationF);
		zone4.addStation(stationG);
		zone4.addStation(stationH);
		zone4.addStation(stationI);
		
		//Network 
		net = new StationNet(zone1);
		
		//Pricer
		pricer = new CustomPricer();
	}
	
@Test
public void shouldTripInZone1Cost240() {
		Integer price  = pricer.price(1, 1);
		assertEquals(240, price);
	}

@Test
public void shouldTripInZone2Cost240() {
	Integer price  = pricer.price(2, 2);
	assertEquals(240, price);
	}

@Test
public void shouldTripFromZone1ToZone2Cost240() {
	Integer price  = pricer.price(1, 2);
	assertEquals(240, price);
}

@Test
public void shouldTripFromZone2ToZone1Cost240() {
	Integer price  = pricer.price(2, 1);
	assertEquals(240, price);
}
//
@Test
public void shouldTripInZone3Cost200() {
	Integer price  = pricer.price(3, 3);
	assertEquals(200, price);
}
@Test
public void shouldTripInZone4Cost200() {
	Integer price  = pricer.price(4, 4);
	assertEquals(200, price);
}

@Test
public void shouldTripFromZone3ToZone4Cost200() {
	Integer price  = pricer.price(3, 4);
	assertEquals(200, price);
}

@Test
public void shouldTripFromZone4ToZone3Cost200() {
	Integer price  = pricer.price(4, 3);
	assertEquals(200, price);
}

//
@Test
public void shouldTripFromZone3ToZone1Cost280() {
	Integer price  = pricer.price(3, 1);
	assertEquals(280, price);
}

@Test
public void shouldTripFromZone3ToZone2Cost280() {
	Integer price  = pricer.price(3,2);
	assertEquals(280, price);
}

//
@Test
public void shouldTripFromZone4ToZone1Cost300() {
	Integer price  = pricer.price(4, 1);
	assertEquals(300, price);
}

@Test
public void shouldTripFromZone4ToZone2Cost300() {
	Integer price  = pricer.price(4, 2);
	assertEquals(300, price);
}

//
@Test
public void shouldTripFromZone1ToZone3Cost280() {
	Integer price  = pricer.price(1, 3);
	assertEquals(280, price);
}

@Test
public void shouldTripFromZone2ToZone3Cost280() {
	Integer price  = pricer.price(2, 3);
	assertEquals(280, price);
}

//
@Test
public void shouldTripFromZone1ToZone4Cost300() {
	Integer price  = pricer.price(1, 4);
	assertEquals(300, price);
}

@Test
public void shouldTripFromZone2ToZone4Cost300() {
	Integer price  = pricer.price(2, 4);
	assertEquals(300, price);
}

@Test
public void shoudTripFromStationAtoStationBCost240AndFromZone1ToZone1() {
	Price price  = pricer.price("A", "B", net);
	assertEquals(240, price.getValue());
	assertEquals(1, price.getStartZone());
	assertEquals(1, price.getEndZone());
}

@Test
public void shouldTripFromStationAtoStationFCost280AndFromZone1ToZone3() {
	Price price  = pricer.price("A", "F", net);
	assertEquals(280, price.getValue());
	assertEquals(1, price.getStartZone());
	assertEquals(3, price.getEndZone());
}

@Test
public void shoudTripFromStationAtoStationGCost300AndFromZone1ToZone4() {
	Price price  = pricer.price("A", "G", net);
	assertEquals(300, price.getValue());
	assertEquals(1, price.getStartZone());
	assertEquals(4, price.getEndZone());
}
	
}
