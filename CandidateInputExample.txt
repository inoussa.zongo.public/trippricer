{
  "taps" : [ {
    "unixTimestamp" : 1,
    "customerId" : 1,
    "station" : "A"
  }, {
    "unixTimestamp" : 2,
    "customerId" : 1,
    "station" : "B"
  },
  {
    "unixTimestamp" : 3,
    "customerId" : 1,
    "station" : "B"
  }, {
    "unixTimestamp" : 4,
    "customerId" : 1,
    "station" : "D"
  },
  {
    "unixTimestamp" : 5,
    "customerId" : 1,
    "station" : "D"
  }, {
    "unixTimestamp" : 6,
    "customerId" : 1,
    "station" : "E"
  },
  {
    "unixTimestamp" : 7,
    "customerId" : 1,
    "station" : "E"
  }, {
    "unixTimestamp" : 8,
    "customerId" : 1,
    "station" : "B"
  },
  
  {
    "unixTimestamp" : 1,
    "customerId" : 2,
    "station" : "C"
  }, {
    "unixTimestamp" : 2,
    "customerId" : 2,
    "station" : "E"
  },
  {
    "unixTimestamp" : 3,
    "customerId" : 2,
    "station" : "E"
  }, {
    "unixTimestamp" : 4,
    "customerId" : 2,
    "station" : "G"
  },
  {
    "unixTimestamp" : 5,
    "customerId" : 2,
    "station" : "G"
  }, {
    "unixTimestamp" : 6,
    "customerId" : 2,
    "station" : "I"
  },
  {
    "unixTimestamp" : 7,
    "customerId" : 2,
    "station" : "I"
  }, {
    "unixTimestamp" : 8,
    "customerId" : 2,
    "station" : "C"
  },

  {
    "unixTimestamp" : 1,
    "customerId" : 3,
    "station" : "F"
  }, {
    "unixTimestamp" : 2,
    "customerId" : 3,
    "station" : "A"
  },
  {
    "unixTimestamp" : 4,
    "customerId" : 3,
    "station" : "F"
  }, {
    "unixTimestamp" : 5,
    "customerId" : 3,
    "station" : "D"
  },

  {
    "unixTimestamp" : 1,
    "customerId" : 4,
    "station" : "G"
  }, {
    "unixTimestamp" : 2,
    "customerId" : 4,
    "station" : "A"
  },
  {
    "unixTimestamp" : 4,
    "customerId" : 4,
    "station" : "G"
  }, {
    "unixTimestamp" : 5,
    "customerId" : 4,
    "station" : "D"
  },

  {
    "unixTimestamp" : 1,
    "customerId" : 5,
    "station" : "A"
  }, {
    "unixTimestamp" : 2,
    "customerId" : 5,
    "station" : "F"
  },
  {
    "unixTimestamp" : 4,
    "customerId" : 5,
    "station" : "D"
  }, {
    "unixTimestamp" : 5,
    "customerId" : 5,
    "station" : "F"
  },

  {
    "unixTimestamp" : 1,
    "customerId" : 6,
    "station" : "A"
  }, {
    "unixTimestamp" : 2,
    "customerId" : 6,
    "station" : "G"
  },
  {
    "unixTimestamp" : 4,
    "customerId" : 6,
    "station" : "D"
  }, {
    "unixTimestamp" : 5,
    "customerId" : 6,
    "station" : "I"
  }
  ]
}